package com.jazed.mcmn.MCMNWorlds.commands.world;

import com.jazed.mcmn.MCMNWorlds.SettingsManager;
import com.jazed.mcmn.MCMNWorlds.commands.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.command.CommandSender;

import static com.jazed.mcmn.MCMNWorlds.util.Messages.msgCommandSender;

public class SetDifficulty implements SubCommand
{
    public boolean onCommand(CommandSender sender, String[] args)
    {
        if (sender.hasPermission("worlds.flag"))
        {
            if (args.length > 1)
            {
                World world = Bukkit.getWorld(args[0].toLowerCase());

                if (world != null)
                {
                    if (!args[1].equalsIgnoreCase("easy") && !args[1].equalsIgnoreCase("normal") &&
                            !args[1].equalsIgnoreCase("hard") && !args[1].equalsIgnoreCase("peaceful"))
                    {
                        msgCommandSender(sender, "&cThe difficulty has to be either &eeasy&c, &enormal&c, &ehard &cor &epeaceful&c!");
                        return false;
                    }

                    world.setDifficulty(Difficulty.valueOf(args[1].toUpperCase()));
                    world.save();

                    msgCommandSender(sender, "&aSet difficulty to &3" + world.getDifficulty().toString().toLowerCase() + "&a for world &3" + world.getName() + "&a.");
                    return true;
                }

                msgCommandSender(sender, "&cInvalid world, make sure the world is loaded.");
                return false;
            }

            if (args.length == 1)
            {
                World world = Bukkit.getWorld(args[0].toLowerCase());

                if (world != null)
                {
                    msgCommandSender(sender, "&aThe difficulty for world &3" + world.getName() + " &ais &3" + world.getDifficulty().toString().toLowerCase() + "&a.");
                    return true;
                }

                msgCommandSender(sender, "&cInvalid world, make sure the world is loaded.");
                return false;
            }

            msgCommandSender(sender, "&cInvalid command usage!");
            msgCommandSender(sender, help());
            return false;
        }

        msgCommandSender(sender, "&cYou do not have permission to perform this command.");

        return false;
    }

    public String help()
    {
        return "&e/world difficulty <world> (easy|normal|hard|peaceful) &7- &3Set the difficulty or get the difficulty for a world.";
    }
}