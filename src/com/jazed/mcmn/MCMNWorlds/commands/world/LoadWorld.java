package com.jazed.mcmn.MCMNWorlds.commands.world;

import com.jazed.mcmn.MCMNWorlds.SettingsManager;
import com.jazed.mcmn.MCMNWorlds.commands.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;
import org.bukkit.command.CommandSender;

import static com.jazed.mcmn.MCMNWorlds.util.Messages.*;

public class LoadWorld implements SubCommand
{
    public boolean onCommand(CommandSender sender, String[] args)
    {
        if (sender.hasPermission("worlds.loadworld"))
        {
            if (args.length > 0)
            {
                Bukkit.getScheduler().scheduleSyncDelayedTask(SettingsManager.getPlugin(), () -> Bukkit.createWorld(WorldCreator.name(args[0])), 1L);
                SettingsManager.getInstance().saveWorld(args[0]);

                msgCommandSender(sender, "&aWorld has been loaded!");
                return true;
            }

            msgCommandSender(sender, "&cInvalid command usage!");
            msgCommandSender(sender, help());
            return false;
        }

        msgCommandSender(sender, "&cYou do not have permission to perform this command.");

        return false;
    }

    public String help()
    {
        return "&e/world load <world> &7- &3Loads the given world.";
    }
}
