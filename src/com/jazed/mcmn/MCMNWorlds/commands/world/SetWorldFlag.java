package com.jazed.mcmn.MCMNWorlds.commands.world;

import com.jazed.mcmn.MCMNWorlds.SettingsManager;
import com.jazed.mcmn.MCMNWorlds.commands.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;

import static com.jazed.mcmn.MCMNWorlds.util.Messages.msgCommandSender;

public class SetWorldFlag implements SubCommand
{
    public boolean onCommand(CommandSender sender, String[] args)
    {
        if (sender.hasPermission("worlds.flag"))
        {
            if (args.length >= 3)
            {
                World world = Bukkit.getWorld(args[0].toLowerCase());

                if (world != null)
                {
                    if (SettingsManager.getInstance().isFlagAvailable(args[1]))
                    {
                        SettingsManager.getInstance().setWorldFlag(world.getName(), args[1].toLowerCase(), args[2].toLowerCase());

                        msgCommandSender(sender, "&aSet flag &3" + args[1] + " &ato &3" + args[2] + " &afor world &3" + world.getName() + "&a.");
                        return true;
                    }

                    msgCommandSender(sender, "&cFlag is not available! Type &e/world flags &cfor available flags.");
                    return false;
                }
                msgCommandSender(sender, "&cInvalid world, make sure the world is loaded.");
                return false;
            }

            msgCommandSender(sender, "&cInvalid command usage!");
            msgCommandSender(sender, help());
            return false;
        }

        msgCommandSender(sender, "&cYou do not have permission to perform this command.");

        return false;
    }

    public String help()
    {
        return "&e/world flag <world> <flag> <value> &7- &3Set a flag for the certain world, or update an existing one.";
    }
}