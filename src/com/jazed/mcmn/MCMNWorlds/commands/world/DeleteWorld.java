package com.jazed.mcmn.MCMNWorlds.commands.world;

import com.jazed.mcmn.MCMNWorlds.SettingsManager;
import com.jazed.mcmn.MCMNWorlds.commands.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

import static com.jazed.mcmn.MCMNWorlds.util.Messages.*;

public class DeleteWorld implements SubCommand
{
    public boolean onCommand(CommandSender sender, String[] args)
    {
        if (sender.hasPermission("worlds.deleteworld"))
        {
            if (args.length > 0)
            {
                if (args[0].equalsIgnoreCase("world"))
                {
                    msgCommandSender(sender, "&cYou can't unload the main world!");
                    return false;
                }

                World world = Bukkit.getWorld(args[0]);

                if (world != null)
                {
                    Bukkit.getScheduler().scheduleSyncDelayedTask(SettingsManager.getPlugin(), () -> {
                        for (Player player : world.getPlayers())
                        {
                            player.teleport(Bukkit.getWorld("world").getSpawnLocation());
                            msgPlayer(player, "&cThe world has been unloaded by an administrator. &3You've been teleported to the main world!");
                        }

                        Bukkit.unloadWorld(world, true);
                    }, 1L);

                    msgCommandSender(sender, "&3Attempting to delete world...");

                    StoreFile del = new StoreFile();

                    try
                    {
                        del.setFile(new File(SettingsManager.getPlugin().getServer().getWorldContainer().getCanonicalPath() + File.separator + world.getName()));
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }

                    if (del.getFile() != null)
                    {
                        Bukkit.getScheduler().scheduleSyncDelayedTask(SettingsManager.getPlugin(), () -> {
                            if (deleteWorldFiles(del.getFile()))
                            {
                                SettingsManager.getInstance().removeWorld(world.getName());
                                msgCommandSender(sender, "&aThe world has been deleted successfully!");
                                return;
                            }

                            msgCommandSender(sender, "&cSomething went wrong while trying to delete the world files!");
                        }, 20L);
                    }

                    return true;
                }

                msgCommandSender(sender, "&cThere was no world found with name &3" + args[0] + "&c.");
                return false;
            }

            msgCommandSender(sender, "&cInvalid command usage!");
            msgCommandSender(sender, help());
            return false;
        }

        msgCommandSender(sender, "&cYou do not have permission to perform this command.");

        return false;
    }

    public String help()
    {
        return "&e/world delete <world> &7- &3Delete a world permanently.";
    }

    private boolean deleteWorldFiles(File path)
    {
        if (path.exists())
        {
            File[] files = path.listFiles();

            if (files != null)
            {
                for (File f : files)
                {
                    if (f.isDirectory())
                    {
                        deleteWorldFiles(f);
                    }
                    else
                    {
                        f.delete();
                    }
                }
            }
        }

        return path.delete();
    }

    class StoreFile
    {
        File f = null;

        public void setFile(File f)
        {
            this.f = f;
        }

        public File getFile()
        {
            return this.f;
        }
    }
}
