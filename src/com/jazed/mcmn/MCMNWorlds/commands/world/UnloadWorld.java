package com.jazed.mcmn.MCMNWorlds.commands.world;

import com.jazed.mcmn.MCMNWorlds.SettingsManager;
import com.jazed.mcmn.MCMNWorlds.commands.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static com.jazed.mcmn.MCMNWorlds.util.Messages.*;

public class UnloadWorld implements SubCommand
{
    public boolean onCommand(CommandSender sender, String[] args)
    {
        if (sender.hasPermission("worlds.unloadworld"))
        {
            if (args.length > 0)
            {
                if (args[0].equalsIgnoreCase("world"))
                {
                    msgCommandSender(sender, "&cYou can't unload the main world!");
                    return false;
                }

                World world = Bukkit.getWorld(args[0]);

                if (world != null)
                {
                    Bukkit.getScheduler().scheduleSyncDelayedTask(SettingsManager.getPlugin(), () -> {
                        for (Player player : world.getPlayers())
                        {
                            player.teleport(Bukkit.getWorld("world").getSpawnLocation());
                            msgPlayer(player, "&cThe world has been unloaded by an administrator. &3You've been teleported to the main world!");
                        }

                        Bukkit.unloadWorld(world, true);

                        SettingsManager.getInstance().removeWorld(world.getName());
                    }, 1L);

                    msgCommandSender(sender, "&aWorld has been unloaded!");
                    return true;
                }

                msgCommandSender(sender, "&cThere was no world found with name &3" + args[0] + "&c.");
                return false;
            }

            msgCommandSender(sender, "&cInvalid command usage!");
            msgCommandSender(sender, help());
            return false;
        }

        msgCommandSender(sender, "&cYou do not have permission to perform this command.");

        return false;
    }

    public String help()
    {
        return "&e/world unload <world> &7- &3Unload a specific world.";
    }
}
