package com.jazed.mcmn.MCMNWorlds.commands.world;

import com.jazed.mcmn.MCMNWorlds.SettingsManager;
import com.jazed.mcmn.MCMNWorlds.commands.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;

import java.util.Map;

import static com.jazed.mcmn.MCMNWorlds.util.Messages.msgCommandSender;

public class GetWorldFlags implements SubCommand
{
    public boolean onCommand(CommandSender sender, String[] args)
    {
        if (sender.hasPermission("worlds.flag"))
        {
            if (args.length > 0)
            {
                World world = Bukkit.getWorld(args[0].toLowerCase());

                if (world != null)
                {
                    Map<String, Object> flags = SettingsManager.getInstance().getFlags(world.getName());

                    if (flags != null)
                    {
                        msgCommandSender(sender, "&aList of flags for world &3" + world.getName() + "&a:");

                        for (Map.Entry<String, Object> flag : flags.entrySet())
                        {
                            msgCommandSender(sender, "&d" + flag.getKey() + "&7: &3" + flag.getValue());
                        }

                        return true;
                    }

                    msgCommandSender(sender, "&cThere aren't any flags set for this world!");
                    return false;
                }

                msgCommandSender(sender, "&cInvalid world, make sure the world is loaded.");
                return false;
            }

            msgCommandSender(sender, "&aList of available flags you can use:");

            StringBuilder sb = new StringBuilder();

            for (String flag : SettingsManager.getInstance().getAvailableFlags())
            {
                if (sb.length() > 0)
                {
                    sb.append(", ");
                }

                sb.append(flag);
            }

            msgCommandSender(sender, "&e" + sb.toString() + ".");
            return true;
        }

        msgCommandSender(sender, "&cYou do not have permission to perform this command.");

        return false;
    }

    public String help()
    {
        return "&e/world flags <world> &7- &3Show the list of flags for the given world.";
    }
}