package com.jazed.mcmn.MCMNWorlds.commands.world;

import com.jazed.mcmn.MCMNWorlds.SettingsManager;
import com.jazed.mcmn.MCMNWorlds.commands.SubCommand;
import com.jazed.mcmn.MCMNWorlds.util.Methods;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.command.CommandSender;

import static com.jazed.mcmn.MCMNWorlds.util.Messages.msgCommandSender;

public class CreateWorld implements SubCommand
{
    public boolean onCommand(CommandSender sender, String[] args)
    {
        if (sender.hasPermission("worlds.createworld"))
        {
            if (args.length >= 3)
            {
                if (!args[1].equalsIgnoreCase("normal") && !args[1].equalsIgnoreCase("end") && !args[1].equalsIgnoreCase("nether"))
                {
                    msgCommandSender(sender, "&cThe world type has to be either &enormal&c, &eend &cor &enether&c!");
                    return false;
                }

                if (!args[2].equals("true") && !args[2].equals("false"))
                {
                    msgCommandSender(sender, "&cThe structures parameter has to be either &etrue &cor &efalse&c.");
                    return false;
                }

                WorldCreator creator = new WorldCreator(args[0]);

                creator.environment(World.Environment.valueOf(args[1].replaceAll("end", "the_end").toUpperCase()));
                creator.generateStructures(Boolean.parseBoolean(args[2]));

                if (args.length > 3)
                {
                    if (!Methods.isNumeric(args[3]))
                    {
                        msgCommandSender(sender, "&cThe seed has to be a valid seed number!");
                        return false;
                    }

                    creator.seed(Long.parseLong(args[3]));
                }

                msgCommandSender(sender, "&3Attempting to generate world...");

                Bukkit.getScheduler().scheduleSyncDelayedTask(SettingsManager.getPlugin(), () -> {
                    Bukkit.createWorld(creator);
                    SettingsManager.getInstance().saveWorld(args[0]);

                    msgCommandSender(sender, "&aSuccessfully created world!");
                    msgCommandSender(sender, "&aWorld: &3" + args[0]);
                    msgCommandSender(sender, "&aEnvironment: &3" + args[1]);
                    msgCommandSender(sender, "&aStructures: &3" + args[2]);
                    msgCommandSender(sender, "&aSeed: &3" + ((args.length > 3) ? args[3] : "none"));
                }, 1L);

                return true;
            }

            msgCommandSender(sender, "&cInvalid command usage!");
            msgCommandSender(sender, help());
            return false;
        }

        msgCommandSender(sender, "&cYou do not have permission to perform this command.");

        return false;
    }

    public String help()
    {
        return "&e/world create <name> " +
                "<normal|end|nether> " +
                "<structures -> true|false> " +
                "(seed) " +
                "&7- &3Creates a new world with the given options.";
    }
}
