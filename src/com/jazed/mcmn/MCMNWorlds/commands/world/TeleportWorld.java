package com.jazed.mcmn.MCMNWorlds.commands.world;

import com.jazed.mcmn.MCMNWorlds.commands.SubCommand;
import com.jazed.mcmn.MCMNWorlds.util.Methods;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static com.jazed.mcmn.MCMNWorlds.util.Messages.msgCommandSender;

public class TeleportWorld implements SubCommand
{
    public boolean onCommand(CommandSender sender, String[] args)
    {
        if (sender.hasPermission("worlds.teleportworld"))
        {
            if (args.length > 0)
            {
                if (!Methods.isPlayerInstance(sender))
                {
                    return false;
                }

                World world = Bukkit.getWorld(args[0]);

                if (world != null)
                {
                    Player p = (Player)sender;

                    if (args.length >= 4)
                    {
                        if (Methods.isNumeric(args[1]) && Methods.isNumeric(args[2]) && Methods.isNumeric(args[3]))
                        {
                            p.teleport(new Location(world, Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3])));

                            msgCommandSender(sender, "&aTeleported to X: &3" + args[1] + "&a Y: &3" + args[2] + "&a Z: &3" + args[3] + " &ain world &3" + world.getName() + "&a.");
                            return true;
                        }

                        msgCommandSender(sender, "&cPlease give valid coordinate numbers!");
                        return false;
                    }

                    p.teleport(world.getSpawnLocation());

                    msgCommandSender(sender, "&aTeleported to the spawn location of world &3" + world.getName() + "&a.");
                    return true;
                }

                msgCommandSender(sender, "&cThere was no world found with name &3" + args[0] + "&c.");
                return false;
            }

            msgCommandSender(sender, "&cInvalid command usage!");
            msgCommandSender(sender, help());
            return false;
        }

        msgCommandSender(sender, "&cYou do not have permission to perform this command.");

        return false;
    }

    public String help()
    {
        return "&e/world tp <world> (x) (y) (z) &7- &3Teleport to the spawn location of the world or given location.";
    }
}

