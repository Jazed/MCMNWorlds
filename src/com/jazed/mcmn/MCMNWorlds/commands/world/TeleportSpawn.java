package com.jazed.mcmn.MCMNWorlds.commands.world;

import com.jazed.mcmn.MCMNWorlds.commands.SubCommand;
import com.jazed.mcmn.MCMNWorlds.util.Methods;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static com.jazed.mcmn.MCMNWorlds.util.Messages.msgCommandSender;

public class TeleportSpawn implements SubCommand
{
    public boolean onCommand(CommandSender sender, String[] args)
    {
        if (sender.hasPermission("worlds.teleportspawn"))
        {
            if (!Methods.isPlayerInstance(sender))
            {
                return false;
            }

            Player p = (Player)sender;

            if (args.length > 0)
            {
                World world = Bukkit.getWorld(args[0]);

                if (world != null)
                {
                    p.teleport(world.getSpawnLocation());

                    msgCommandSender(sender, "&aYou've teleported to the spawn location of world &3" + world.getName() + "&a.");
                    return true;
                }

                msgCommandSender(sender, "&cThere was no world found with name &3" + args[0] + "&c.");
                return false;
            }

            p.teleport(p.getLocation().getWorld().getSpawnLocation());

            msgCommandSender(sender, "&aYou've teleported to the spawn location.");
            return false;
        }

        msgCommandSender(sender, "&cYou do not have permission to perform this command.");

        return false;
    }

    public String help()
    {
        return "&e/world spawn &7- &3Teleport to the spawn location of the current world.";
    }
}
