package com.jazed.mcmn.MCMNWorlds.commands.world;

import com.jazed.mcmn.MCMNWorlds.commands.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;

import static com.jazed.mcmn.MCMNWorlds.util.Messages.msgCommandSender;

public class ShowWorlds implements SubCommand
{
    public boolean onCommand(CommandSender sender, String[] args)
    {
        if (sender.hasPermission("worlds.list"))
        {
            StringBuilder sb = new StringBuilder();

            for (World world : Bukkit.getWorlds())
            {
                if (sb.length() > 0)
                {
                    sb.append(", ");
                }

                sb.append(world.getName());
            }

            msgCommandSender(sender, "&aCurrent world list: &3" + sb.toString());
            return true;
        }

        msgCommandSender(sender, "&cYou do not have permission to perform this command.");

        return false;
    }

    public String help()
    {
        return "&e/world list &7- &3Shows the current loaded worlds.";
    }
}