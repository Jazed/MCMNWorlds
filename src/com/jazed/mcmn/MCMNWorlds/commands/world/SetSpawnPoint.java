package com.jazed.mcmn.MCMNWorlds.commands.world;

import com.jazed.mcmn.MCMNWorlds.commands.SubCommand;
import com.jazed.mcmn.MCMNWorlds.util.Methods;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static com.jazed.mcmn.MCMNWorlds.util.Messages.*;

public class SetSpawnPoint implements SubCommand
{
    public boolean onCommand(CommandSender sender, String[] args)
    {
        if (sender.hasPermission("worlds.setspawn"))
        {
            if (!Methods.isPlayerInstance(sender))
            {
                return false;
            }

            Player p = (Player)sender;
            Location l = p.getLocation();

            if (Bukkit.getWorld(l.getWorld().getName()).setSpawnLocation(l.getBlockX(), l.getBlockY(), l.getBlockZ()))
                msgPlayer(p, "&aYou've set the spawn point for world &3" + l.getWorld().getName() + "&a.");
            else
                msgPlayer(p, "&cSomething went wrong while trying to set the world, please try again.");

            return true;
        }

        msgCommandSender(sender, "&cYou do not have permission to perform this command.");

        return false;
    }

    public String help()
    {
        return "&e/world setspawn &7- &3Sets the spawn point for the current world to your location.";
    }
}
