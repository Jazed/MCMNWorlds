package com.jazed.mcmn.MCMNWorlds.events;

import com.jazed.mcmn.MCMNWorlds.SettingsManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockChangeEvents implements Listener
{
    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockBreakEvent(BlockBreakEvent event)
    {
        Player p = event.getPlayer();

        if (!SettingsManager.getInstance().isBlockBreak(p.getLocation().getWorld().getName()) && !p.hasPermission("world.break"))
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockPlaceEvent(BlockPlaceEvent event)
    {
        Player p = event.getPlayer();

        if (!SettingsManager.getInstance().isBlockPlace(p.getLocation().getWorld().getName()) && !p.hasPermission("world.place"))
            event.setCancelled(true);
    }
}
