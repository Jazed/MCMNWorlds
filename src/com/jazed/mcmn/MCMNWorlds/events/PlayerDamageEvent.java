package com.jazed.mcmn.MCMNWorlds.events;

import com.jazed.mcmn.MCMNWorlds.SettingsManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

public class PlayerDamageEvent implements Listener
{
    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDamageEvent(EntityDamageEvent event)
    {
        if (!(event.getEntity() instanceof Player))
            return;

        if (SettingsManager.getInstance().isInvincible(event.getEntity().getWorld().getName()))
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent event)
    {
        if (!(event.getEntity() instanceof Player))
        {
            return;
        }

        if (!SettingsManager.getInstance().isPVP(event.getEntity().getWorld().getName()) && (event.getDamager() instanceof Player))
            event.setCancelled(true);
    }
}