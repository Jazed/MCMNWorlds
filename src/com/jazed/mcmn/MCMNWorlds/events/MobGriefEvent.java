package com.jazed.mcmn.MCMNWorlds.events;

import com.jazed.mcmn.MCMNWorlds.SettingsManager;
import org.bukkit.entity.Enderman;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

public class MobGriefEvent implements Listener
{
    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityExplodeEvent(EntityExplodeEvent event)
    {
        if (!SettingsManager.getInstance().isMobGrief(event.getLocation().getWorld().getName()))
        {
            event.blockList().clear();
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityChangeBlockEvent(EntityChangeBlockEvent event)
    {
        if (!(event.getEntity() instanceof Enderman))
        {
            return;
        }

        if (!SettingsManager.getInstance().isMobGrief(event.getEntity().getWorld().getName()))
        {
            event.setCancelled(true);
        }
    }
}
