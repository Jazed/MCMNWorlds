package com.jazed.mcmn.MCMNWorlds.events;

import com.jazed.mcmn.MCMNWorlds.SettingsManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

public class WeatherChange implements Listener
{
    @EventHandler(priority = EventPriority.NORMAL)
    public void onWeatherChangeEvent(WeatherChangeEvent event)
    {
        if (!SettingsManager.getInstance().isWeatherFlag(event.getWorld().getName()) && event.toWeatherState())
        {
            event.setCancelled(true);
        }
    }
}