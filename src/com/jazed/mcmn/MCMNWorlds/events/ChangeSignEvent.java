package com.jazed.mcmn.MCMNWorlds.events;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

public class ChangeSignEvent implements Listener
{
    @EventHandler(priority = EventPriority.NORMAL)
    public void onSignChangeEvent(SignChangeEvent event)
    {
        for (int i = 0; i < event.getLines().length; i++)
            event.setLine(i, ChatColor.translateAlternateColorCodes('&', event.getLine(i)));
    }
}
