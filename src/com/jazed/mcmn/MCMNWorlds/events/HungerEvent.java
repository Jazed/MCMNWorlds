package com.jazed.mcmn.MCMNWorlds.events;

import com.jazed.mcmn.MCMNWorlds.SettingsManager;
import com.jazed.mcmn.MCMNWorlds.util.Methods;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class HungerEvent implements Listener
{
    @EventHandler(priority = EventPriority.NORMAL)
    public void onFoodLevelChangeEvent(FoodLevelChangeEvent event)
    {
        if ((!SettingsManager.getInstance().isHunger(event.getEntity().getWorld().getName()) ||
                SettingsManager.getInstance().isInvincible(event.getEntity().getWorld().getName())) &&
                Methods.isPlayerInstance(event.getEntity()))
        {
            Player p = (Player)event.getEntity();

            if (p.getFoodLevel() < 20)
                p.setFoodLevel(20);
                p.setSaturation(20);

            event.setCancelled(true);
        }
    }
}
