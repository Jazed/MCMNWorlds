package com.jazed.mcmn.MCMNWorlds;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public class SettingsManager
{
    private static SettingsManager instance = new SettingsManager();
    private static Plugin plugin;

    private File systemFile;

    private FileConfiguration system;

    private ArrayList<String> flags = new ArrayList<>(Arrays.asList(new String[]{"weather-change", "pvp", "invincible", "mob-grief",
            "hunger", "block-break", "block-place"}));

    public static SettingsManager getInstance()
    {
        return instance;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void setup(Plugin p)
    {
        plugin = p;

        File folder = p.getDataFolder();
        this.systemFile = new File(plugin.getDataFolder(), "system.yml");

        try
        {
            if (!folder.exists())
            {
                folder.mkdir();
            }

            if (!this.systemFile.exists())
            {
                this.systemFile.createNewFile();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        reloadSystem();
    }

    public static Plugin getPlugin()
    {
        return plugin;
    }

    public void saveSystem()
    {
        try
        {
            this.system.save(this.systemFile);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public FileConfiguration getSystemConfig()
    {
        return this.system;
    }

    public void reloadSystem()
    {
        this.system = YamlConfiguration.loadConfiguration(this.systemFile);
    }

    public void saveWorld(String world)
    {
        ArrayList<String> worlds = (this.system.isSet("worlds")) ? new ArrayList<>(this.system.getStringList("worlds")) : new ArrayList<>();

        if (!worlds.contains(world))
        {
            worlds.add(world);

            this.system.set("worlds", worlds);

            saveSystem();
        }
    }

    public void removeWorld(String world)
    {
        ArrayList<String> worlds = (this.system.isSet("worlds")) ? new ArrayList<>(this.system.getStringList("worlds")) : new ArrayList<>();

        if (worlds.size() > 0 && worlds.contains(world))
        {
            worlds.remove(world);

            this.system.set("worlds", worlds);

            saveSystem();
        }
    }

    public ArrayList<String> getWorlds()
    {
        ArrayList<String> worlds = (this.system.isSet("worlds")) ? new ArrayList<>(this.system.getStringList("worlds")) : new ArrayList<>();

        if (worlds.size() > 0)
        {
            return worlds;
        }

        return null;
    }

    private boolean isFlag(String world, String flag)
    {
        return !this.system.isSet("world." + world + "." + flag) || ((this.system.getString("world." + world + "." + flag).equals("true")));
    }

    public boolean isWeatherFlag(String world)
    {
        return isFlag(world, "weather-change");
    }

    public boolean isInvincible(String world)
    {
        return isFlag(world, "invincible");
    }

    public boolean isPVP(String world)
    {
        return isFlag(world, "pvp");
    }

    public boolean isMobGrief(String world)
    {
        return isFlag(world, "mob-grief");
    }

    public boolean isHunger(String world)
    {
        return isFlag(world, "hunger");
    }

    public boolean isBlockBreak(String world)
    {
        return isFlag(world, "block-break");
    }

    public boolean isBlockPlace(String world)
    {
        return isFlag(world, "block-place");
    }

    public void setWorldFlag(String world, String flag, Object value)
    {
        this.system.set("world." + world + "." + flag, value);

        saveSystem();
        reloadSystem();
    }

    public boolean isFlagAvailable(String flag)
    {
        return this.flags.contains(flag);
    }

    public ArrayList<String> getAvailableFlags()
    {
        return this.flags;
    }

    public Map<String, Object> getFlags(String world)
    {
        if (this.system.isSet("world." + world))
        {
            return this.system.getConfigurationSection("world." + world).getValues(false);
        }

        return null;
    }
}