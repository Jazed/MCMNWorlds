package com.jazed.mcmn.MCMNWorlds;

import com.jazed.mcmn.MCMNWorlds.commands.SubCommand;
import com.jazed.mcmn.MCMNWorlds.commands.world.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static com.jazed.mcmn.MCMNWorlds.util.Messages.*;

public class CommandHandler implements CommandExecutor
{
    private Plugin p;

    private HashMap<String, SubCommand> commands = new HashMap<>();
    private String command;

    public CommandHandler(Plugin p, String command)
    {
        this.p = p;
        this.command = command;

        loadCommands();
    }

    private void loadCommands()
    {
        this.commands.put("setspawn", new SetSpawnPoint());
        this.commands.put("load", new LoadWorld());
        this.commands.put("create", new CreateWorld());
        this.commands.put("delete", new DeleteWorld());
        this.commands.put("unload", new UnloadWorld());
        this.commands.put("list", new ShowWorlds());
        this.commands.put("tp", new TeleportWorld());
        this.commands.put("flag", new SetWorldFlag());
        this.commands.put("flags", new GetWorldFlags());
        this.commands.put("difficulty", new SetDifficulty());
        this.commands.put("spawn", new TeleportSpawn());
    }

    public boolean onCommand(CommandSender sender, Command cmd1, String cmdLabel, String[] args)
    {
        String cmd = cmd1.getName();

        if (cmd.equalsIgnoreCase(this.command))
        {
            if (args == null || args.length < 1)
            {
                msgCommandSender(sender, "&a&lMCMNWorlds by Jazed");
                msgCommandSender(sender, "&6Type /" + this.command + " help for help.");

                return true;
            }

            String sub = args[0];

            if (sub.equalsIgnoreCase("help"))
            {
                help(sender);
                return true;
            }

            ArrayList<String> list = new ArrayList<>(Arrays.asList(args));
            list.remove(0);

            args = list.toArray(new String[list.size()]);

            if (!this.commands.containsKey(sub))
            {
                msgCommandSender(sender, "&cCommand doesn't exist! Please type /" + this.command + " help for help.");
                return true;
            }

            try
            {
                this.commands.get(sub).onCommand(sender, args);
            }
            catch (Exception e)
            {
                System.out.println(getPrefix() + colorMsg("&cError while executing command."));
                e.printStackTrace();

                msgCommandSender(sender, "&cAn error occured while executing the command.");
                msgCommandSender(sender, "&cType /" + this.command + " help for help.");
            }

            return true;
        }

        return false;
    }

    public void help(CommandSender sender)
    {
        msgCommandSender(sender, "&e&lMCMNWorlds commands &7- &3<required> (optional)");

        for (SubCommand cmd : this.commands.values())
            msgCommandSender(sender, cmd.help().replaceAll(" &7- &3", "\n            &3"));
    }
}