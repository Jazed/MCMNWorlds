package com.jazed.mcmn.MCMNWorlds;

import com.jazed.mcmn.MCMNWorlds.events.*;
import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class MCMNWorlds extends JavaPlugin
{
    private String command = "world";

    public void onEnable()
    {
        PluginManager pm = this.getServer().getPluginManager();

        pm.registerEvents(new PlayerJoin(), this);
        pm.registerEvents(new WeatherChange(), this);
        pm.registerEvents(new PlayerDamageEvent(), this);
        pm.registerEvents(new MobGriefEvent(), this);
        pm.registerEvents(new HungerEvent(), this);
        pm.registerEvents(new ChangeSignEvent(), this);
        pm.registerEvents(new BlockChangeEvents(), this);

        SettingsManager.getInstance().setup(this);

        this.getCommand("world").setExecutor(new CommandHandler(this, this.command));

        loadWorlds();
    }

    private void loadWorlds()
    {
        Bukkit.getScheduler().scheduleSyncDelayedTask(SettingsManager.getPlugin(), () -> {
            ArrayList<String> worlds = SettingsManager.getInstance().getWorlds();

            if (worlds != null)
            {
                for (String world : worlds)
                {
                    Bukkit.getServer().createWorld(WorldCreator.name(world));
                    System.out.println("[MCMNWorlds] Loaded world: " + world);
                }
            }
        }, 1L);
    }
}