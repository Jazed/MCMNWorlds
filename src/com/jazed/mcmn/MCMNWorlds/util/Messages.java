package com.jazed.mcmn.MCMNWorlds.util;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Messages
{
    private static String prefix = ChatColor.GOLD + "[Worlds] ";

    public static void msgPlayer(Player player, String message)
    {
        player.sendMessage(getPrefix() + colorMsg(message));
    }

    public static void msgCommandSender(CommandSender sender, String message)
    {
        sender.sendMessage(getPrefix() + colorMsg(message));
    }

    public static String colorMsg(String message)
    {
        return message.replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
    }

    public static String cleanString(String message)
    {
        return ChatColor.stripColor(message);
    }

    public static String getPrefix()
    {
        return prefix;
    }
}