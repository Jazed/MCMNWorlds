package com.jazed.mcmn.MCMNWorlds.util;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Methods
{
    public static boolean isNumeric(String number)
    {
        try
        {
            long l = Long.parseLong(number);
        }
        catch (NumberFormatException e)
        {
            return false;
        }

        return true;
    }

    public static boolean isPlayerInstance(CommandSender sender)
    {
        return (sender instanceof Player);
    }
}
