<snippet>
  <content>
# MCMNWorlds
## Installation
Place MCMNWorlds.jar into plugins folder and run.
## Commands

`[required] (optional)`

* /world create `[name] [normal|end|nether] [structures = true|false] (seed)` - Creates a new world with the given options.
* /world delete `[world]` - Delete a world permanently.
* /world flags `[world]` - Show the list of flags for the given world.
* /world load `[world]` - Loads the given world.
* /world difficulty `[world] (easy|normal|hard|peaceful)` - Set the difficulty or get the difficulty for a world.
* /world setspawn - Sets the spawn point for the current world to your location.\n
* /world flag `[world] [flag] [value]` - Set a flag for the certain world, or update an existing one.
* /world list - Shows the current loaded worlds.
* /world spawn - Teleport to the spawn location of the current world.\n
* /world tp `[world] (x) (y) (z)` - Teleport to the spawn location of the world or given location.
* /world unload `[world]` - Unload a specific world.

## Licensing

Please see the file called LICENSE.

</content>
</snippet>